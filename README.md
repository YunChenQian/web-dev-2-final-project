# Web Dev 2 Final Project
By Chen Yun Qian and Johakim Fontaine Barboza

## Contents
The project contains 1 html file, a regular and mobile css and 2 javascript files

`game_settings.js` contains all of the logic for the game and the leaderboard

`utilities.js` contains utilities functions like createElement()

## How to run
1. Open page.html in browser
2. Type in a name
3. Select grid size / color option / difficulty level
4. Press on the "Start Puzzle Game" button
5. Select the correct cells
6. Press on "Submit Guesses"

Thank you nasr and badr
