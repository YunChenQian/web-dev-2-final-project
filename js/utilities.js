/* eslint-disable no-unused-vars */
/*global createTableFromArrayObjects*/
/*global createMyElement*/
// We disn't know how to access functions defined in other file so we disabled it
'use strict';

/**
 * @description creates an element with the given tag, class, and id if it exists and 
 * attaches it to the parent element
 * @param {String} tag 
 * @param {String} class_ 
 * @param {HTMLElement} parent 
 * @param {String} id 
 * @returns returns the created element
 */
function createMyElement(tag, class_, parent, id)
{
    let createElem = document.createElement(tag);
    createElem.className = class_;
    if (id) createElem.id = id;
    parent.appendChild(createElem);
    return createElem; 
}

/**
 * @description creates an table element with the given object array and attaches it to the parent element
 * @param {Object[]} gameSessionArray 
 * @param {HTMLElement} parent 
 */
function createTableFromArrayObjects(gameSessionArray, parent){
    //creates table
    let myTable = createMyElement('table', 'scoreBoardTable', parent);

    //gets the keys of array
    let keys = Object.keys(gameSessionArray[0]);

    //creates the table header with data and attaches the event listeners to the table headers
    let tableHeader = createMyElement('tr', 'scoreBoardHeaders', myTable);
    let count = 0;
    keys.forEach((key) =>
    {
        let countClone = JSON.parse(JSON.stringify(count));
        let tableData = createMyElement('th', 'scoreBoardData', tableHeader);
        tableData.textContent = key;
        tableData.addEventListener("click", function(){
          
          sortTable(countClone, myTable)
        });
        count ++;
    });

    //loop insert all data into the table
    for (let i of gameSessionArray)
    {
        let myRow = createMyElement('tr', 'scoreBoardtr', myTable);
        keys.forEach((key) => {
            let myData = createMyElement('td', 'scoreBoardtd', myRow);
            myData.textContent = i[key];
        });
    }
}

/**
 * @description sorts the table based on the given column index and changes the sorting type depending on the contained cells
 * @param {Number} n //the index of the column to be sorted
 * @param {HTMLElement} table 
 */
function sortTable(n, table){
    if(table.rows[0].getElementsByTagName("TH")[n].textContent == 'player' || table.rows[0].getElementsByTagName("TH")[n].textContent == 'level'){
        sortTableString(n, table);
    }
    else
    {
        sortTableNum(n, table);
    }
}

/**
 * @description sorts the table in ascending or descending order depending on how the column is already sorted with numbers
 * @param {Number} n index of the column to be sorted
 * @param {HTMLElement} table 
 */
function sortTableNum(n, table) {
    var rows, switching, i, x, y, shouldSwitch, switchcount = 0;
    switching = true;
    let dir = "asc";
    // while loop to switch the rows depending on the value of the cell
    while (switching) {
      switching = false;
      rows = table.rows;
      for (i = 1; i < (rows.length - 1); i++) {
        shouldSwitch = false;
        x = rows[i].getElementsByTagName("TD")[n];
        y = rows[i + 1].getElementsByTagName("TD")[n];
        let xClone = parseInt(JSON.parse(JSON.stringify(x.textContent)));
        let yClone = parseInt(JSON.parse(JSON.stringify(y.textContent)));
        if (dir == "asc") {
          if (xClone > yClone) {
            shouldSwitch = true;
            break;
          }
        } else if (dir == "desc") {
          if (xClone < yClone) {
            shouldSwitch = true;
            break;
          }
        }
      }
      if (shouldSwitch) {
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        switchcount ++;
      } else {
        //if the direction is already switched to ascending order, we will switch to descending order and go through the loop again
        if (switchcount == 0 && dir == "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
  }

/**
 * @description sorts the table in ascending or descending order depending on how the column is already sorted with strings
 * @param {Number} n index of the column to be sorted
 * @param {HTMLElement} table   
 */
function sortTableString(n, table) {
    var rows, switching, i, x, y, shouldSwitch, switchcount = 0;
    switching = true;
    let dir = "asc";
    // while loop to switch the rows depending on the value of the cell
    while (switching) {
      switching = false;
      rows = table.rows;
      for (i = 1; i < (rows.length - 1); i++) {
        shouldSwitch = false;
        x = rows[i].getElementsByTagName("TD")[n];
        y = rows[i + 1].getElementsByTagName("TD")[n];
        if (dir == "asc") {
          if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        } else if (dir == "desc") {
          if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        }
      }
      if (shouldSwitch) {
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        switchcount ++;
      } else {
        //if the direction is already switched to ascending order, we will switch to descending order and go through the loop again
        if (switchcount == 0 && dir == "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
  }
