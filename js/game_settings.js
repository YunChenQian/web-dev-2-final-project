/* eslint-disable no-undef */
/*global createMyElement*/
// We disn't know how to access functions defined in other file so we disabled it
'use strict';

/**
 * @description -generateTable(size, color, color_ammount, difficulty)
 * generateTable creates a table with exactly size rows and columns.
 * it also sets the colors for the tds according to the difficulty and the main color picked
 * 
 * @param {int} size 
 * @param {string} color 
 * @param {int} color_ammount this is the number of cells that are the main color
 * @param {string} difficulty 
 */
function generateTable(size, color, color_ammount, difficulty) {
    let trs = document.getElementById("table-render-space");
    
    let previous_table_style;
    let previous_td_style;
    
    // if the table exists, remove it and save its style
    if (trs.children[0] != null) {
        let table = trs.children[0];
        let td = document.getElementsByTagName("td")[0];
        // Here, we need the cssText to extract a string
        previous_table_style = table.style.cssText;
        previous_td_style = td.style.cssText;

        trs.removeChild(trs.children[0]);
    }

    let table = createMyElement("table", "game-table", trs);
    
    // Using setAttribute allows us to overwrite whatever style the table currently has
    table.setAttribute("style", previous_table_style);

    // This is for keeping track of the cells and assigning ids to them
    // It will later be used for when we shuffle the cells arround
    let cells = [];
    let count = 0;

    for (let i = 0; i < size; i++) {
        let tr = createMyElement("tr", "game-tr", table);
        for (let j = 0; j < size; j++) {
            let td = createMyElement("td", "game-td" ,tr , "" + count);

            // This is fore when the user clicks on the cells
            td.addEventListener("click", () =>{
                highlightTable(td);
            });
            
            // The ids and index are pushed to cells for later
            cells.push(count);

            td.setAttribute("style", previous_td_style);
            // Set the background color of all cells to NOT the main color (and the coresponding number)
            // Do this based on the given difficulty range
            if (color == "red") {
                let not_red = randomIntFromInterval(0, 1);
                if (not_red == 0) {
                    let rgb = generateColor("blue", difficulty);
                    td.style.backgroundColor = "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")";
                    td.innerText = "2";
                }
                if (not_red == 1) {
                    let rgb = generateColor("green", difficulty);
                    td.style.backgroundColor = "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")";
                    td.innerText = "1";
                }
            }
            if (color == "green") {
                let not_green = randomIntFromInterval(0, 1);
                if (not_green == 0) {
                    let rgb = generateColor("red", difficulty);
                    td.style.backgroundColor = "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")";
                    td.innerText = "0";
                }
                if (not_green == 1) {
                    let rgb = generateColor("blue", difficulty);
                    td.style.backgroundColor = "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")";
                    td.innerText = "2";
                }
            }
            if (color == "blue") {
                let not_blue = randomIntFromInterval(0, 1);
                if (not_blue == 0) {
                    let rgb = generateColor("red", difficulty);
                    td.style.backgroundColor = "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")";
                    td.innerText = "0";
                }
                if (not_blue == 1) {
                    let rgb = generateColor("green", difficulty);
                    td.style.backgroundColor = "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")";
                    td.innerText = "1";
                }
            }
            // Now that all cells have a color that ISNT the main color, we create a shuffled array of length color_ammount
            // Afterwards we assign the main color to those indexes in the cells[] from the shuffled array
            if (i == size-1 && j == size-1) {
                let shuffled = cells.sort(() => .5 - Math.random()).slice(0, color_ammount);
                console.log("Lenght of shuffled is " + shuffled.length);
                for (let cell of shuffled) {
                    console.log(cell);
                    let rgb = generateColor(color, difficulty);
                    document.getElementById(cell).style.backgroundColor = "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")";
                    document.getElementById(cell).innerText = color == "red" ? 0 : color == "green" ? 1 : 2;
                }
            }
            count++
        }
    }
}
/**
 * @description -highlightTable(cell)
 * Updates the border of the cells when clicked on
 * Adds the clicked class if it isn't already selected
 * Else, remove the border and the clicked class
 * @param {HTMLElement} cell 
 */
function highlightTable(cell) {
    if (!cell.style.border || cell.style.border == "") {
        cell.style.border = "4px solid #fff700";
        cell.classList.add("clicked");
    }
    else if (cell.style.border == "4px solid #fff700") {
        cell.style.border = null;
        cell.classList.remove("clicked");
    }
}
/**
 * @description -startButtonEnableDisable()
 * Controls whether the start button is clickable
 * Depending on if the name is between 4-12 characters and is only alphanum
 * Depending on if the size of the table is bigger than 1
 */
function startButtonEnableDisable() {
    let button = document.getElementById("start-game");
    let disabled = true;
    let pattern = /^[a-zA-Z0-9]{4,12}$/;
    if (pattern.test(document.getElementById("pName").value)){
        disabled = false;
        document.getElementById("pName").style.backgroundColor = '#90ee90';
        if (document.getElementById("size").value == "" || document.getElementById("size").value < 2){
            disabled = true;
        }
        else{
            disabled = false;
        }
    }
    else{
        disabled = true;
        // Warning message if your name does not match criteria
        document.getElementById("pName").style.backgroundColor = '#FFA500';
        document.getElementById("pName").placeholder = "min 4 max 12 alphanumeric";
    }
    
    button.disabled = disabled;
}
/**
 * @description calculates the success rate of the users guesses
 * @param {string} color 
 * @returns {string} successRate
 */
function successRate(color){
    let correct = 0;
    let clicked = document.getElementsByClassName("clicked"); // the clicked cells
    for (let i = 0; i < clicked.length; i++) {
        let rgb = clicked[i].style.backgroundColor;
        // Split the rgb string into an array
        rgb = rgb.substring(4, rgb.length-1).replace(/ /g, '').split(',');
        let red = parseInt(rgb[0]);
        let green = parseInt(rgb[1]);
        let blue = parseInt(rgb[2]);
        if (color == "red" && red > green && red > blue) {
            correct++;
        }
        else if(color == "green" && green > red && green > blue) {
            correct++;
        }
        else if(color == "blue" && blue > green && blue > red) {
            correct++;
        }
    }
    let number;

    // if there is more clicked cells than the amount we need to guess, we will use the clicked amount
    // to calculate the success rate. Else we use the amount we need to guess to calcuate the success rate
    if (parseInt(document.getElementById('nmb-guesses').innerText.split(" ")[0]) < clicked.length) {
        number = clicked.length;
    }
    else {
        number = parseInt(document.getElementById('nmb-guesses').innerText.split(" ")[0]);
    }

    let success = correct/number * 100;

    // play sounds depending on success rate
    if (success == 100)
    {
        let sound = new Audio("../sounds/win.mp3");
        sound.play();
    }
    else if (success < 50){
        let sound = new Audio("../sounds/lose.mp3");
        sound.play();
    }
    // Make sure success rate is a 2 decimnal position number
    return success.toFixed(2);
}

/**
 * @description a class that simply creates a player (aka a row in the leaderboard)
 * @param {string} playerName 
 * @param {number} dur 
 * @param {number} clicks 
 * @param {string} level 
 * @param {number} success 
 * @param {number} rowxcol 
 */
class createPlayerFunction {
    constructor(playerName, dur, clicks, level, success, rowxcol) {
        this.player = playerName;
        this.dur = dur;
        this.clicks = clicks;
        this.level = level;
        this.success = success;
        this.rowxcol = rowxcol;
    }
}
/**
 * @description After getting all the parameters for the player constructor (name, dur, clicks, level, rowxcol, success)
 * we call the player constructor and creates the player and add it to the playerinfoArray and we return the playerInfoArray
 * @param {Object[]} playerInfoArray 
 * @returns {Object[]} playerInfoArray
 */
function createPlayerArray(playerInfoArray,){
    let color_picker = document.getElementsByClassName('radio-container');
    let color; 
    // gets the clicked value of radio element
    for (let i = 0; i < color_picker.length; i++) {
        if (color_picker[i].children[1].checked) {
            color = color_picker[i].children[1].value;
        }
    }
    let playerName = document.getElementById("pName").value.toLowerCase();
    let timerNums = document.getElementById("timerElement").innerText.split(":");
    let timerPrint;
    //calculates the duration of the game, if the minute is greater than 0 then we will 
    let dur = (parseInt(timerNums[0]*60) + parseInt(timerNums[1])) + " s";
    let clicks = document.getElementsByClassName("clicked").length;
    let level = document.getElementById('difficulty').value;
    let rowxcol = document.getElementById("size").value;
    let success = successRate(color, Math.floor(rowxcol*rowxcol/2));
    playerInfoArray.push(new createPlayerFunction(playerName, dur, clicks, level, success, rowxcol));
    return playerInfoArray;
}
/**
 * from https://stackoverflow.com/questions/4959975/generate-random-number-between-two-numbers-in-javascript
 * @param {number} min 
 * @param {number} max 
 * @returns {number} randomNumber
 */
function randomIntFromInterval(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min)
}
/**
 * @description generates rgb values based on a given main color and a difficulty range
 * @param {string} color 
 * @param {string} difficulty 
 * @returns {number[]} rgb array
 */
function generateColor(color, difficulty) {
    let min, max;
    if (difficulty == "easy") {
        min = 0;
        max = 255;
    }
    if (difficulty == "medium") {
        min = randomIntFromInterval(0, 255-80);
        max = min + 80;
    }
    if (difficulty == "difficult") {
        min = randomIntFromInterval(0, 255-50);
        max = min + 50;
    }
    if (difficulty == "brutal") {
        min = randomIntFromInterval(0, 255-10);
        max = min + 10;
    }

    if (color == "red") {
        let r = randomIntFromInterval(min, max);

        let g = randomIntFromInterval(min, r-1);
        let b = randomIntFromInterval(min, r-1);

        return [r, g, b];
    }
    if (color == "green") {
        let g = randomIntFromInterval(min, max);

        let r = randomIntFromInterval(min, g-1);
        let b = randomIntFromInterval(min, g-1);

        return [r, g, b];
    }
    if (color == "blue") {
        let b = randomIntFromInterval(min, max);

        let r = randomIntFromInterval(min, b-1);
        let g = randomIntFromInterval(min, b-1);

        return [r, g, b];
    }
}
/**
 * @description shows rgb values on the cells instead of the numbers
 * @param {Event} event 
 */
function show_rgb(event) {
    let cells = document.querySelectorAll(".game-td");
   
    if (event.target.checked == true) {
        for (let i = 0; i < cells.length; i++) { 
            cells[i].innerText = cells[i].style.backgroundColor;
        }
    }
    else {
        for (let i = 0; i < cells.length; i++) {
            let rgb = cells[i].style.backgroundColor.match(/\d+/g);
            // convert the values of rgb to int so they can be properly compared
            rgb = rgb.map((x) => {
                return parseInt(x);
            })

            if (rgb[0] > rgb[1] && rgb[0] > rgb[2]) {
                cells[i].innerText = 0;
            }
            if (rgb[1] > rgb[0] && rgb[1] > rgb[2]) {
                cells[i].innerText = 1;
            }
            if (rgb[2] > rgb[0] && rgb[2] > rgb[1]) {
                cells[i].innerText = 2;
            }
        }
    }
}

/**
 * @description Creates the game based on the inputs from the form 
 */
function startGame() {
    let size = document.getElementById('size').value;
    let color_picker = document.getElementsByClassName('radio-container');
    let color;
    let color_ammount;
    let difficulty = document.getElementById('difficulty').value;
    
    for (let i = 0; i < color_picker.length; i++) {
        if (color_picker[i].children[1].checked) {
            color = color_picker[i].children[1].value;
            // This can be changed to whatever fixed ammount you want
            color_ammount = randomIntFromInterval(1, (size*size) -1);

            // This is the text in the game rules
            let color_text = document.getElementById('gameDescptionSpan');
            color_text.innerText = color;
            color_text.style.backgroundColor = color;

            // This is the text that displays how many cells you need to click
            let color_ammount_text = document.getElementById('nmb-guesses');
            color_ammount_text.innerText = color_ammount + " " + color; 
        }
    }
    generateTable(size, color, color_ammount, difficulty);
    document.getElementById("submit-guesses").disabled = false;
}
/**
 * @description Saves the info of the leaderboard into local storage
 * @param {Object[]} playerInfoArray 
 */
function saveInfo(playerInfoArray) {
    console.log(playerInfoArray);
    let count = 0;
    for (let row of playerInfoArray) {
        localStorage.setItem(count, JSON.stringify(row));
        count++;
    }
}
/**
 * @description Load the info of the leaderboard from local storage
 * @param {Object[]} playerInfoArray 
 */
function loadInfo(playerInfoArray) {
    playerInfoArray = [];
    for (let i = 0; i < localStorage.length; i++) {
        playerInfoArray[i] = JSON.parse(localStorage[i]);
    }
    if(document.getElementsByClassName('scoreBoardTable')[0] != null){
        document.getElementById('leaderboard').lastChild.remove();
    }
    createTableFromArrayObjects(playerInfoArray, document.getElementById("leaderboard"));
    return playerInfoArray;
}
/**
 * @description Clears local storage with alert confirm
 */
function clearInfo() {
    const response = confirm("Are you sure you want to clear local storage?");
    if (response) {
        alert("Local storage cleared");
        localStorage.clear();
    } 
    else {
        alert("Action cancelled");
    }
}
/**
 * @description Main function responsible for intitializing the game and adding event listeners on the form/buttons
 */
function createGame() {
    // default value
    document.getElementById("submit-guesses").disabled = true;
    document.getElementById('red').checked = true;
    let playerInfoArray = [];
    let start_button =  document.getElementById('start-game');
    let timer;
    start_button.addEventListener("click", () => {
        startGame(playerInfoArray);
        //method to start the timer
        clearInterval(timer);
        let sec = 0, min = 0;
        timer  = setInterval(() => {
        sec++;
        if (sec >= 60) {
            min++;
            sec = 0;
        }
        document.getElementById("timerElement").innerText = min + ":" + sec;
    }, 1000);

    });
    start_button.disabled = true;

    let cheat_button = document.querySelectorAll('input[type="checkbox"')[0];
    cheat_button.addEventListener('change', show_rgb);

    document.getElementById("size").addEventListener('change', startButtonEnableDisable);
    document.getElementById("pName").addEventListener('input', startButtonEnableDisable);
    //turns off the submit button and adds objects to the array and makes the leaderboard and changes the colors
    document.getElementById("submit-guesses").addEventListener("click", (event) => {
        if(document.getElementsByClassName('scoreBoardTable')[0] != null){
            document.getElementById('leaderboard').lastChild.remove();
        }
        //shows the rgb of the cells
        let cells = document.querySelectorAll(".game-td");
        for (let i = 0; i < cells.length; i++) { 
            cells[i].innerText = cells[i].style.backgroundColor;
        }
        event.target.disabled = true;
        createPlayerArray(playerInfoArray);
        createTableFromArrayObjects(playerInfoArray, document.getElementById("leaderboard"));
        clearInterval(timer);
    });
    
    let save_button = document.getElementById('saveButton');
    save_button.addEventListener('click', () => saveInfo(playerInfoArray));

    let load_button = document.getElementById('loadButton');
    load_button.addEventListener('click', () => {
        playerInfoArray = loadInfo(playerInfoArray)});

    let clear_button = document.getElementById('clearButton');
    clear_button.addEventListener('click', clearInfo);
}

createGame();
